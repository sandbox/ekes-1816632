<?php

/**
 * @file
 * Plugin to provide access control based upon node UUID.
 */

$plugin = array(
  'title' => t("Node: UUID"),
  'description' => t('Control access by an node UUID.'),
  'callback' => 'uuid_node_ctools_access_check',
  'default' => array('uuid' => array()),
  'settings form' => 'uuid_node_ctools_access_settings',
  'summary' => 'uuid_node_ctools_access_summary',
  'required context' => array(
    new ctools_context_required(t('Node'), 'node'),
  ),
);

/**
 * Settings form for the UUID condition.
 */
function uuid_node_ctools_access_settings(&$form, &$form_state, $conf) {
  $form['settings']['uuid'] = array(
    '#type' => 'textfield',
    '#default_value' => $conf['uuid'],
    '#title' => t('UUID'),
  );
}

/**
 * Check for access.
 */
function uuid_node_ctools_access_check($conf, $context) {
  if (empty($context[0]) || empty($context[0]->data) || empty($context[0]->data->uuid)) {
    return FALSE;
  }
  return $context[0]->data->uuid === $conf['uuid'];
}

/**
 * Provide a summary description.
 */
function uuid_node_ctools_access_summary($conf, $context, $plugin) {
  return t('Node UUID is %uuid', array('%uuid' => $conf['uuid']));
}
